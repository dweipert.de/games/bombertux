extends Node


func get_level_position(scene):
	var x = floor((scene.position.x + 8) / 16)
	var y = floor((scene.position.y + 8) / 16)
	
	return Vector2(x, y)


func get_level_position_grid(scene):
	return self.get_level_position(scene) * 16
