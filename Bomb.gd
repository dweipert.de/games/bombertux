extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var Explosion = preload("res://Explosion.tscn")


# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimatedSprite.play()
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_Timer_timeout():
	print("BYE BOMB")
	$CollisionShape2D.disabled = true
	
	var space_state = get_world_2d().direct_space_state
	var base_position = self.position
	var power = 5
	
	var directions = [
		Vector2.UP,
		Vector2.RIGHT,
		Vector2.DOWN,
		Vector2.LEFT,
	]
	
	for j in range(directions.size()):
		for i in range(power):
			var direction = directions[j]
			if direction.length() == 0:
				continue
			
			var from = base_position + (direction * i * 16)
			var to = base_position + (direction * (i + 1) * 16)
			
			var result = space_state.intersect_ray(from, to)
			if result and result.collider:
				if "Bomb" in result.collider.name:
					result.collider._on_Timer_timeout()
					break
				
				# don't add explosion when Damageable?
				#continue
				
				# break when hitting wall
				#break
				
				pass
			
			var explosion = Explosion.instance()
			explosion.position = to
			get_tree().get_current_scene().add_child(explosion)
	
	var explosion = Explosion.instance()
	explosion.position = base_position
	get_tree().get_current_scene().add_child(explosion)
	
	queue_free()


func _on_Area2D_body_exited(body):
	if body.name == "Player":
		self.set_collision_layer_bit(0, true)
	
