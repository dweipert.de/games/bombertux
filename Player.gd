extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var SPEED = 60
var velocity = Vector2(0, 0)
var Bomb = preload("res://Bomb.tscn")


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_key_pressed(KEY_LEFT):
		velocity.x -= SPEED
		#$AnimatedSprite.rotation = 90.0
		#$AnimatedSprite.flip_v = true
		$AnimatedSprite.play("left")
	if Input.is_key_pressed(KEY_RIGHT):
		velocity.x += SPEED
		#$AnimatedSprite.rotation = 90.0
		#$AnimatedSprite.flip_v = false
		$AnimatedSprite.play("right")
	if Input.is_key_pressed(KEY_UP):
		velocity.y -= SPEED
		#$AnimatedSprite.rotation = 0.0
		#$AnimatedSprite.flip_v = false
		$AnimatedSprite.play("up")
	if Input.is_key_pressed(KEY_DOWN):
		velocity.y += SPEED
		#$AnimatedSprite.rotation = 0.0
		#$AnimatedSprite.flip_v = true
		$AnimatedSprite.play("down")
	
	if velocity.x < 0 && velocity.y < 0:
		$AnimatedSprite.play("tl")
	elif velocity.x > 0 && velocity.y < 0:
		$AnimatedSprite.play("tr")
	elif velocity.x < 0 && velocity.y > 0:
		$AnimatedSprite.play("bl")
	elif velocity.x > 0 && velocity.y > 0:
		$AnimatedSprite.play("br")
	
	if Input.is_action_just_pressed("ui_accept"):
		self.plant_bomb()
	
	#if velocity.length() > 0:
		#$AnimatedSprite.play()
	
	velocity = move_and_slide(velocity, Vector2.UP)
	velocity.x = lerp(velocity.x, 0, 1)
	velocity.y = lerp(velocity.y, 0, 1)


func plant_bomb():
	print("HI BOMB")
	var bomb = Bomb.instance()
	bomb.position = Utilities.get_level_position_grid(self)
	#bomb.position = self.position
	get_tree().get_current_scene().add_child(bomb)
